<?php

namespace Drupal\number_display;

class Settings extends Base {
  protected $field;
  protected $instance;
  protected $view_mode;
  protected $form;
  protected $form_state;

  static function hook_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
    $data = get_defined_vars();

    if ('number_display_advanced' != $instance['display'][$view_mode]['type']) {
      return null;
    }

    $item = static::create($data)->doFieldFormatterSettingsForm();
    return $item;
  }

  function doFieldFormatterSettingsForm() {
    $form = [];
    $form += $this->formThousandSeparator();
    $form += $this->formDecimalSeparator();
    $form += $this->formScale();
    $form += $this->formPrefixSuffix();
    $form += $this->formRound();
    $form += $this->formRate();
    return $form;
  }

  protected function formThousandSeparator() {
    $index = 'thousand_separator';
    $form[$index] = [
      '#type' => 'select',
      '#title' => t('Thousand marker'),
      '#options' => Options::getThousandSeparator(),
      '#default_value' => $this->getSetting($index),
    ];

    return $form;
  }

  protected function formDecimalSeparator() {
    $index = 'decimal_separator';
    $form['decimal_separator'] = [
      '#type' => 'select',
      '#title' => t('Decimal marker'),
      '#options' => Options::getDecimalSeparator(),
      '#default_value' => $this->getSetting($index),
    ];
    return $form;
  }

  protected function formScale() {
    $index = 'scale';
    $form[$index] = [
      '#type' => 'select',
      '#title' => t('Scale'),
      '#options' => Options::getScale(),
      '#default_value' => $this->getSetting($index),
      '#description' => t('The number of digits to the right of the decimal.'),
    ];

    return $form;
  }

  protected function formPrefixSuffix() {
    $index = 'prefix_suffix';
    $form[$index] = [
      '#type' => 'checkbox',
      '#title' => t('Display prefix and suffix.'),
      '#default_value' => $this->getSetting($index),
    ];

    return $form;
  }

  protected function formRound() {
    $index = 'round';
    $form[$index] = [
      '#type' => 'select',
      '#title' => t('Round'),
      '#default_value' => $this->getSetting($index),
      '#options' => Options::getRound(),
    ];

    return $form;
  }

  protected function formRate() {
    $index = 'rate';
    $form[$index] = [
      '#type' => 'select',
      '#title' => t('Rate'),
      '#default_value' => $this->getSetting($index),
      '#options' => Options::getRate(),
    ];

    return $form;
  }

  protected function getSetting($index) {
    $settings = &$this->instance['display'][$this->view_mode]['settings'];
    $item = isset($settings[$index]) ? $settings[$index] : null;
    return $item;
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = ['field_formatter_settings_form'];
    $items = [];

    foreach ($hooks as $name) {
      $items[$class]['hook_' . $name] = $module . '_' . $name;
    }

    return $items;
  }

}
