<?php

namespace Drupal\number_display;

use Drupal\mixin\Traits\Hook;

class Base {
  use Hook;

  function __construct(array &$data = []) {
    foreach (array_keys(get_object_vars($this)) as $name) {
      if (isset($data[$name])) {
        $this->$name = &$data[$name];
      }
    }
  }

  static function create(array &$data = []) {
    $item = new static($data);
    return $item;
  }

}
