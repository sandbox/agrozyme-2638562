<?php

namespace Drupal\number_display;

use Drupal\mixin\Decimal;

class View extends Base {
  protected $entity_type;
  protected $entity;
  protected $field;
  protected $instance;
  protected $langcode;
  protected $items;
  protected $display;
  protected $view_mode;

  static function hook_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
    $data = get_defined_vars();
    $item = static::create($data)->doFieldFormatterView();
    return $item;
  }

  function doFieldFormatterView() {
    $form = [];

    foreach ($this->items as $delta => $item) {
      $form[$delta] = [
        '#markup' => $this->doFormat($item['value'], $this->display['settings'], $this->instance['settings']),
      ];
    }

    return $form;
  }

  function doFormat($data, array $formatter_settings, array $instance_settings = []) {
    $formatter_settings = $this->mergeFormatterSettings($formatter_settings);
    $value = round(Decimal::create(10)->power($formatter_settings['rate'])->multiply($data)->toFloat(),
      $formatter_settings['scale'],
      $formatter_settings['round']);

    $output = number_format($value,
      $formatter_settings['scale'],
      $formatter_settings['decimal_separator'],
      $formatter_settings['thousand_separator']);

    if (0 < $formatter_settings['rate']) {
      $rateOptions = Options::getRate();
      $output .= $rateOptions[$formatter_settings['rate']];
    }

    if ($formatter_settings['prefix_suffix']) {
      $prefix = $this->getPrefixSuffix('prefix', $data, $instance_settings);
      $suffix = $this->getPrefixSuffix('suffix', $data, $instance_settings);
      $output = $prefix . $output . $suffix;
    }

    return $output;
  }

  protected function mergeFormatterSettings(array $settings) {
    if (false == isset($settings['scale'])) {
      $settings['scale'] = 0;
    }

    if (false == isset($settings['decimal_separator'])) {
      $settings['decimal_separator'] = '';
    }

    if (false == isset($settings['thousand_separator'])) {
      $settings['thousand_separator'] = ',';
    }

    if (false == isset($settings['round'])) {
      $settings['round'] = PHP_ROUND_HALF_UP;
    }

    if (false == isset($settings['rate'])) {
      $settings['rate'] = 0;
    }

    return $settings;
  }

  protected function getPrefixSuffix($index, $value, array $settings = []) {
    $setting = &$settings[$index];
    $items = isset($setting) ? array_map('field_filter_xss', explode('|', $setting)) : [''];
    $data = (count($items) > 1) ? format_plural($value, $items[0], $items[1]) : $items[0];
    return $data;
  }

  static function hook_field_formatter_settings_summary($field, $instance, $view_mode) {
    $data = get_defined_vars();
    $item = static::create($data)->doFieldFormatterSettingsSummary();
    return $item;
  }

  function doFieldFormatterSettingsSummary() {
    $display = $this->instance['display'][$this->view_mode];
    $value = 1234.1234567890;
    $items = [];

    if ('number_display_advanced' == $display['type']) {
      $items[] = t('Value: @item', ['@item' => $value]);
      $items[] = t('Format: @item',
        ['@item' => $this->doFormat($value, $display['settings'], $this->instance['settings'])]);
    }

    return implode('<br />', $items);
  }

  static function format($data, array $formatter_settings, array $instance_settings = []) {
    $item = static::create()->doFormat($data, $formatter_settings, $instance_settings);
    return $item;
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = ['field_formatter_settings_summary', 'field_formatter_view'];
    $items = [];

    foreach ($hooks as $name) {
      $items[$class]['hook_' . $name] = $module . '_' . $name;
    }

    return $items;
  }

}
