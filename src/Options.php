<?php

namespace Drupal\number_display;

use Drupal\mixin\Traits\Object;

class Options {
  use Object;

  static function getThousandSeparator() {
    $items = [
      '' => t('<none>'),
      '.' => t('Decimal point'),
      ',' => t('Comma'),
      ' ' => t('Space'),
    ];
    return $items;
  }

  static function getDecimalSeparator() {
    $items = ['.' => t('Decimal point'), ',' => t('Comma')];
    return $items;
  }

  static function getScale() {
    $items = drupal_map_assoc(range(0, 10));
    return $items;
  }

  static function getRound() {
    $items = [
      PHP_ROUND_HALF_UP => 'HALF UP',
      PHP_ROUND_HALF_DOWN => 'HALF DOWN',
      PHP_ROUND_HALF_EVEN => 'HALF EVEN',
      PHP_ROUND_HALF_ODD => 'HALF ODD',
    ];

    return $items;
  }

  static function getRate() {
    $items = [
      0 => t('None'),
      2 => t('%'),
      3 => t('‰'),
      4 => t('‱'),
      6 => t('ppm'),
      9 => t('ppb'),
      12 => t('ppt'),
    ];

    return $items;
  }

  static function getDefaultFormatterSettings() {
    $item = [
      'thousand_separator' => '',
      'decimal_separator' => '.',
      'scale' => 0,
      'prefix_suffix' => true,
      'round' => PHP_ROUND_HALF_UP,
      'rate' => 0,
    ];

    return $item;
  }
}
